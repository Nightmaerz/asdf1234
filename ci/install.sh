
sudo apt-get install bzr cmake compiz-dev gnome-common libbamf3-dev libboost-dev 
libboost-serialization-dev libgconf2-dev libgdu-dev libglewmx1.6-dev 
libgnome-desktop-3-dev libibus-1.0-dev libindicator3-dev libjson-glib-dev 
libnotify-dev libnux-2.0-dev libpci-dev libsigc++-2.0-dev libunity-dev 
libunity-misc-dev libutouch-geis-dev libxxf86vm-dev libzeitgeist-dev xsltproc

bzr branch lp:nux
cd nux
./autogen.sh --disable-examples --disable-gputests --disable-tests --prefix="$PREFIX"
make -j4
make install

bzr branch lp:unity
cd unity
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug -DCOMPIZ_PLUGIN_INSTALL_TYPE=local -DGSETTINGS_LOCALINSTALL=ON -DCMAKE_INSTALL_PREFIX="$PREFIX"
make -j4
make install
